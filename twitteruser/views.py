from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .helpers import twitter_helper


class TwitterScreen(APIView):

    def get(self, request, format="json", screen_name=None):
        count = request.query_params.get('count', 10)
        if count:
            try:
                count = int(count)
            except:
                count = 10
        response = twitter_helper.get_tweets_for_screen(screen_name, count)
        return Response(response)