from .views import TwitterScreen
from django.conf.urls import url, include

app_name='twitteruser'

urlpatterns = [
    url(r'^twittscreen/(?P<screen_name>[A-Za-z0-9_]+)/$', TwitterScreen.as_view(), name="twitts"),
]