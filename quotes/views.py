# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
import requests
from django.http import JsonResponse
from django.core.cache import cache
from bsedata.bse import BSE


def convertToTemplate(response):
    tempRes = {}
    tempRes["prev"] = float(response["previousClose"])
    tempRes["open"] = float(response["previousOpen"])
    tempRes["low"] = float(response["dayLow"])
    tempRes["high"] = float(response["dayHigh"])
    tempRes["last"] = float(response["currentValue"])
    tempRes["change"] = response["change"]
    tempRes["changeper"] = response["pChange"]
    tempRes["date"] = response["updatedOn"]
    volume_str = response["totalTradedQuantity"].split()
    print(response["totalTradedQuantity"])
    volume = 0
    if volume_str[0]:
        volume = float(volume_str[0]) * 100000
    
    tempRes["vol"] = volume
    return tempRes

def home(request):
    scriptcode = request.GET.get('scriptcode').upper()
    baseurl = 'http://datafet.com/api_india.php'

    if scriptcode:
        data = cache.get(scriptcode)
        if data is not None:
            return JsonResponse(data)
        else:
            print(scriptcode)
            if scriptcode.isdigit():
                b = BSE()
                q = b.getQuote(scriptcode)
                finalData = convertToTemplate(q)
                print(finalData)
                return JsonResponse(finalData)
            else:
                payload = {'client': 'shivaprasad2452', 'code': scriptcode}
                response = requests.get(baseurl,params=payload)
                if response.status_code == requests.codes.ok:
                    stockData = response.json()
                    finalData = mapJson(stockData)
                else:
                    return raise_for_status()
                        
            cache_duration = 180
            if scriptcode == 'NIFTY' or scriptcode == 'SENSEX':
                cache_duration = 60
            print("this is fuck")
            cache.add(scriptcode, finalData, cache_duration)
            print(JsonResponse(finalData))
            return JsonResponse(finalData)
    else:
        return HttpResponseBadRequest()

def mapJson( basicStockResponse ):
   last = basicStockResponse['last']
   prev = basicStockResponse['prev']
   change = last - prev
   changeper = 100 * (last - prev)/prev
   basicStockResponse['change'] = format(change, '.2f')
   basicStockResponse['changeper'] = format(changeper, '.2f')
   return basicStockResponse
