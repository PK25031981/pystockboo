# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
import requests
from django.http import JsonResponse
from django.core.cache import cache
from rest_framework.views import APIView
from rest_framework.response import Response
import logging, logging.config
import sys
import json

class LotSizeInfo(APIView):
    def mapJson(self, basicStockResponse ):
        outPutMap = {}
        optionInfo = {}
        optionInfo['annualisedVolatility'] = basicStockResponse['data'][0]['annualisedVolatility']
        optionInfo['pChange'] = basicStockResponse['data'][0]['pChange']
        optionInfo['pChange'] = basicStockResponse['data'][0]['pChange']
        optionInfo['marketLot'] = basicStockResponse['data'][0]['marketLot']
        optionInfo['prevClose'] = basicStockResponse['data'][0]['prevClose']
        optionInfo['expiryDate'] = basicStockResponse['data'][0]['expiryDate']
        optionInfo['lastPrice'] = basicStockResponse['data'][0]['lastPrice']

        outPutMap['data'] = optionInfo
        return outPutMap

    def get(self, request, format="json"):
        symbol = request.GET.get('symbol')
        expiry = request.GET.get('expiry')
        print(symbol,expiry)
        baseurl = 'https://www.nseindia.com/live_market/dynaContent/live_watch/get_quote/ajaxFOGetQuoteJSON.jsp'
        payload = {'underlying': symbol, 'instrument':'FUTIDX', 'expiry': expiry}
        headers = {"Referer":"https://www.nseindia.com","User-Agent":"Mozilla/5.0","Accept":"/"}

        strikePrices = requests.get(baseurl,params=payload,headers=headers)
        print(strikePrices.url)
        print(strikePrices.text)
        if strikePrices.status_code == requests.codes.ok:
            stockData = strikePrices.json()
            outJson = self.mapJson(stockData)
            return JsonResponse(outJson)
        else:
            return raise_for_status()
