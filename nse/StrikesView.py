# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
import requests
from django.http import JsonResponse
from django.core.cache import cache
from rest_framework.views import APIView
from rest_framework.response import Response
import logging, logging.config
import sys
import json
logger = logging.getLogger(__name__)

class StrikePrice(APIView):

    def get(self, request, format="json"):
        logger.info('Information incoming!')
        symbol = request.GET.get('symbol')
        expiry = request.GET.get('expiry')
        print(symbol,expiry)
        baseurl = 'https://www.nseindia.com/live_market/dynaContent/live_watch/get_quote/ajaxFOGetQuoteDataTest.jsp'
        payload = {'i':'OPTIDX','u': symbol, 'e': expiry, 'o': "CE", 'k': 'CE'}
        strikePrices = requests.get(baseurl,params=payload)
        print(strikePrices.url)
        print(strikePrices.json())
        print(strikePrices.json)
        if strikePrices.status_code == requests.codes.ok:
            return Response(strikePrices)
        else:
            return raise_for_status()

    def mapJson( optionIndexRes, optionSTKRes):
        response_data = {}
        data_dict = {}
        data_dict['OPTIDX'] = optionIndexRes.json()
        data_dict['OPTSTK'] = optionSTKRes.json()
        response_data['data'] = data_dict
        print(response_data)
        return JsonResponse(response_data)
