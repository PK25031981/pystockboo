# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
import requests
from django.http import JsonResponse
from django.core.cache import cache
from rest_framework.views import APIView
from rest_framework.response import Response
import logging, logging.config
import sys
import json

class NSEExpiraryInfo(APIView):

    def get(self, request, format="json", screen_name=None):
        nseOptionsExpiryDateURL = 'https://www.nseindia.com/live_market/dynaContent/live_watch/get_quote/ajaxFOGetQuoteDataTest.jsp?i=OPTIDX&u=NIFTY'
        optionIndexRes = requests.get(nseOptionsExpiryDateURL)

        if optionIndexRes.status_code == requests.codes.ok:
            nseOptionsExpiryDateURL = 'https://www.nseindia.com/live_market/dynaContent/live_watch/get_quote/ajaxFOGetQuoteDataTest.jsp?i=OPTSTK&u=TCS'
            optionSTKRes = requests.get(nseOptionsExpiryDateURL)
            return mapJson(optionIndexRes,optionSTKRes)
        else:
            return raise_for_status()

def mapJson( optionIndexRes, optionSTKRes):
    response_data = {}
    data_dict = {}
    data_dict['OPTIDX'] = optionIndexRes.json()
    data_dict['OPTSTK'] = optionSTKRes.json()
    response_data['data'] = data_dict
    print(response_data)
    return JsonResponse(response_data)
